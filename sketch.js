var data;
var wh ;
var hh ;
var cloudSize;
var sideSpace;
var barSize;
var gapSize;
var dataYear = 2015; // set this to different years if you want different datasets
var cloudImages = [];
var zeroPoint;
var buttonOpacity = 0;
var rainVideo;
var font;
var currentData;
var dataSet;
var cloudOpacity = 0;
var cloudTitleOpacity = cloudOpacity;
var transitionPhase = false;
var transitionVal =0;
var changedData = false;
var lightningOpacity = 0;
var thunder =[]


function preload() {
    data = loadJSON("assets/rainfall.json");
    rainVideo = createVideo("assets/rain.mp4");
    font = loadFont("assets/timeburnernormal.ttf")
    thunder = [loadSound("assets/thunder.mp3"),loadSound("assets/thunder2.mp3"),
              loadSound("assets/thunder3.mp3"),]
    rainVideo.hide();
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    // this line is required due to a bug in the current version of p5
    // https://github.com/processing/p5.js/issues/2154
    data = Object.values(data);
    // any additional setup code goes here

    thisYearsData = data.filter(o => o.year == dataYear); // filters to the current year

    // filters the data for each location
    ainslieData = thisYearsData.filter(o => o.location == "ainslie");
    arandaData = thisYearsData.filter(o => o.location == "aranda");
    torrensData = thisYearsData.filter(o => o.location == "torrens");
    queanbeyanData = thisYearsData.filter(o => o.location == "queanbeyan");
    parliamentHouseData = thisYearsData.filter(o => o.location == "parliament-house");
    botanicGardensData = thisYearsData.filter(o => o.location == "botanic-gardens");
    dataSet = [ainslieData,arandaData,torrensData,queanbeyanData,parliamentHouseData,botanicGardensData];

    //initialising first few useful variables
    currentData = 0;
    wh = width/2;
    hh = height/2;

    cloudSize = width - width/4;
    sideSpace = (width-cloudSize)/2;
    barSize = cloudSize/13;
    gapSize = (cloudSize/12)/10;
    zeroPoint = hh-hh/4;

    //video plays and repeats
    rainVideo.loop();
    noStroke();
}

var startOpacity = 255;
function draw() {
    // your "draw loop" code goes here
    image(rainVideo,0,0,width,height);

    //if statement for the beginning of the program
    if (millis() < 6000 && millis()>3000){
      startOpacity -= 2;
    }

    drawCloud1();
    drawButton();
    storm();

    dataSet[currentData].map(o => drawData(o));

    // if statement for the change location transition
    if (transitionPhase && millis() < now+750) {
      transitionVal+= 13;
    } else if (transitionPhase && millis() >= now+750 && millis() < now+2000) {
      if (!changedData){
        currentData = (currentData + 1) % (dataSet.length-1);
        changedData = true;
      }
      if (transitionVal > 0){
        transitionVal-= 13;
      } else if (transitionVal <= 0){
        transitionVal = 0;
        transitionPhase = false;
        changedData = false;
      }
      cloudTitleOpacity+=15;
    }

    //used for thunder lighting effect
    fill(255,lightningOpacity);
    rect(0,0,width,height);

    //used for the start screen
    fill(0,startOpacity)
    rect(0,0,width,height);
    fill(255,startOpacity-50)
    textAlign(CENTER);
    textFont(font,38);
    text("ACT Rainfall in " + dataYear, wh,hh);
    textFont(font,20);
    text("measured in millimeters", wh,hh+30);
}

//draws the cloud and the months on the cloud
function drawCloud1(){
    rectMode(CENTER);
    fill(200,254);

    rect(wh,zeroPoint - hh/15, cloudSize+wh/4,2*hh/15,100,100);
    rect(wh,zeroPoint - 3*hh/15, cloudSize,2*hh/15+10,100,100);
    rect(wh,zeroPoint - 5*hh/15, cloudSize-wh/4,2*hh/15+10,100,100);
    rect(wh,zeroPoint - 7*hh/15, cloudSize-2*wh/4,2*hh/15+10,100,100);
    rect(wh,zeroPoint - 9*hh/15, cloudSize-3*wh/4,2*hh/15+10,100,100);
    ellipse(wh,zeroPoint +10- 9*hh/15, (cloudSize-3*wh/4)/2,150);

    triangle(wh,hh/5,sideSpace+cloudSize-50,zeroPoint,sideSpace+50,zeroPoint)
    //ellipse(wh,zeroPoint-hh/2, 200,200);
    rectMode(CORNER);

    // makes the clouds details transition out if player walks outd
    if (mouseY < zeroPoint) {
      cloudOpacity += 3;
      cloudTitleOpacity += 3;
    } else {
      cloudOpacity -= 10;
      cloudTitleOpacity -= 7;
    }

    if (cloudOpacity >= 240 ){
      cloudOpacity = 240;
    } else if (cloudOpacity <= 0){
      cloudOpacity = 0;
    }

    if (cloudTitleOpacity >= 240 ){
      cloudTitleOpacity = 240;
    } else if (cloudTitleOpacity <= 0){
      cloudTitleOpacity = 0;
    }

    //draws the months on the cloud
    fill(140,cloudOpacity);
    textAlign(CENTER);
    textFont(font,25);
    var months = ["Ja","Fe","Mr","Ap","My","Jn","Jl","Au","Se","Oc","No","De"];
    for (i = 0; i < 12; i++){
      text(months[i],(barSize-wh/20)/2+sideSpace+(barSize+gapSize)*i, zeroPoint-10);
    }

}

//draws the bar data alongside the title and values
function drawData(o){
  textSize(50);
  fill(100,cloudTitleOpacity);
  text(o.location, wh,zeroPoint-hh/2);


  fill(6, 176, 181, 230);
  if (o.rainfall*3 - transitionVal <= 0) {
      rect(sideSpace+(barSize+gapSize)*(o.month-1), zeroPoint , barSize-wh/20,0,0,0,100);
  } else {
    rect(sideSpace+(barSize+gapSize)*(o.month-1), zeroPoint , barSize-wh/20,o.rainfall*3-transitionVal,0,0,100);
  }

  textAlign(CENTER);
  textFont(font,17);
  textStyle(BOLD);
  fill(255,cloudOpacity);
  text(o.rainfall,(barSize-wh/20)/2+sideSpace+(barSize+gapSize)*(o.month-1),zeroPoint+o.rainfall*3-20);
}

//draws the button on the bottom that changes the data location
function drawButton(){
  strokeWeight(3);
  stroke(255);
  line(wh-20,height-30,wh,height-10);
  line(wh+20,height-30,wh,height-10);

  noStroke();

  if (buttonOpacity >= 100){
    buttonOpacity = 100;
  } else if (buttonOpacity <= 0){
    buttonOpacity = 0;
  }

  if (mouseY > height-50){
    buttonOpacity += 2;
  } else {
    buttonOpacity--;
  }

  fill(255,buttonOpacity);
  rect(0,height-50,width,height);
}

//function called when mouse is pressed
function mousePressed(){
  if (mouseY > height - 50){
    now = millis();
    transitionPhase = true;
  } else if (mouseY < zeroPoint && !stormPlaying){
    nowStorm = millis();
    random(thunder).play();
    stormPlaying = true;

  }
}

var nowStorm;
var stormPlaying;

//function called when player calls the storm 
function storm(){
  if (millis() > nowStorm && millis() < nowStorm +100){
    lightningOpacity = 255;
  } else if (millis() > nowStorm+300 && millis() < nowStorm+350){
      lightningOpacity = 255;
  } else if (millis() > nowStorm+900 && millis() < nowStorm+1000){
      lightningOpacity = 255;
  } else if (millis()>nowStorm+1000 && millis() < nowStorm+5000){
      lightningOpacity-=2;
  } else if (millis()>nowStorm+5000 && stormPlaying){
      stormPlaying = false;
  }  else {
    lightningOpacity =0;
  }
}
