# Artist Statement

When I was a child, I was always excited about rain. It had always brought a sense of calm and comfort to me. Often I would stare outside the window, following the little water droplets racing down as my mind slowly drifted away. Rain has the same affect on me even to this day. When I need to focus, sleep or calm down, I would play some rain noises. Thus, as a tribute, I had chosen the rainfall dataset.

I owe a lot to rain, and would love for more people to learn more about it. In my datavis, I tried to portray that same sense of calmness to the users. By using mute and cool colours, simple round shapes, smooth transitions and a background of rain droplets on a window seal, I hope to make users comfortable with rain. I also added a thunder strike to make users comfortable with it. Once users are comfortable, they will be more open to learning about rain through the datasets I have processed.
