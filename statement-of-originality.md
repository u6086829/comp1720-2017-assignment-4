# Statement of Originality

I <INSERT YOUR NAME HERE> declare that everything I have submitted in this
assignment is entirely my own work, with the following exceptions:

## Inspiration
I had taken inspiration from those 10 hour Rain White Noise videos on youtube and the sense of calm they bring.

## Code
To embed the video of the rain in the background, I had taken code from p5 js reference. https://p5js.org/examples/dom-video-canvas.html

## Assets

For the background video I had taken an excerpt of the "Rain on a window (2 hours) - sleep sounds" video by youtube channel "Meditation Vacation". (https://www.youtube.com/watch?v=hEZZiLPesgE)

Thunder Sounds taken from:
https://www.youtube.com/watch?v=T-BOPr7NXME
https://www.youtube.com/watch?v=XkzNaD26j-Q
https://www.youtube.com/watch?v=49ucbz56OgI

I chose the rainfall dataset provided with the assignment 4 template repo.
